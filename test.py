from typing import Dict, Any, Callable, Iterable

DataType = Iterable[Dict[str, Any]]
ModifierFunc = Callable[[DataType], DataType]

friends = [
    {'name': 'Sam', 'gender': 'male', 'sport': 'Basketball'},
    {'name': 'Emily', 'gender': 'female', 'sport': 'Volleyball'}
]


def query(data: DataType, selector: ModifierFunc, *filters: ModifierFunc) -> DataType:
    """
    Query data with column selection and filters

    :param data: List of dictionaries with columns and values
    :param selector: result of `select` function call
    :param filters: Any number of results of `field_filter` function calls
    :return: Filtered data
    """
    result = []
    for row in data:
        row = selector(row)
        for field_filter in filters:
            if not field_filter(row):
                continue
        else:
            result.append(row)
        return result


def select(*columns: str) -> ModifierFunc:
    """Return function that selects only specific columns from dataset"""

    def inner(data):
        return {k: v for k, v in data.items() if k in columns}

    return inner


# def field_filter(column: str, *values: Any) -> ModifierFunc:
def field_filter(column: str, *values: Any) -> ModifierFunc:
    """Return function that filters specific column to be one of `values`"""
    def inner(row):
        #return {k: v for k, v in row.items() if k in column}
        return row[column] in values

    return inner


def test_query():
    friends = [
        {'name': 'Sam', 'gender': 'male', 'sport': 'Basketball'},
        {'name': 'Emily', 'gender': 'female', 'sport': 'Volleyball'}
    ]
    value = list(query(
        friends,
        select(*('name', 'gender', 'sport')),
        #field_filter(*('sport', *('Basketball', 'Volleyball'))),
        #field_filter(*('gender', *('female',))),
    ))

    #assert [{'gender': 'female', 'name': 'Emily', 'sport': 'Volleyball'}] == value
    return value


if __name__ == "__main__":
    test_query()
print(test_query())
